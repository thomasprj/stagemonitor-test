package com.mgd.monitoring.controller

import com.mgd.monitoring.worker.RandomResponseTimeWorker
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping

@RestController
class RootController {

    @Autowired RandomResponseTimeWorker worker

    @RequestMapping("/")
    String index() {
        worker.generateDelay()
        return "Hello from the Stagemonitor test application!"
    }
}
