package com.mgd.monitoring.worker

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class RandomResponseTimeWorker {

    private static final Logger log = LoggerFactory.getLogger(RandomResponseTimeWorker.class)

    void generateDelay() {

        Random rand = new Random();

        // get a random value between 1 and 5000
        int  timer = rand.nextInt(5000) + 1

        log.info("Generating delayed response of ${timer} ms...")
        try {
            Thread.sleep(timer)
        } catch (InterruptedException e) {
            log.error(e.printStackTrace())
        }
    }
}
