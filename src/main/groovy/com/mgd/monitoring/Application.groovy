package com.mgd.monitoring

import com.codahale.metrics.annotation.Counted
import com.mgd.monitoring.worker.RandomResponseTimeWorker
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.scheduling.annotation.Scheduled

import org.stagemonitor.core.Stagemonitor
import org.stagemonitor.alerting.check.MetricValueType
import org.stagemonitor.alerting.annotation.SLA
import org.stagemonitor.tracing.Traced

import org.slf4j.Logger
import org.slf4j.LoggerFactory

@SpringBootApplication
class Application {

    @Autowired RandomResponseTimeWorker worker

    private static final Logger log = LoggerFactory.getLogger(Application.class)

    static void main(String[] args) {
        Stagemonitor.init()
        SpringApplication.run(Application.class, args)
    }

    @Scheduled(initialDelay = 10000l, fixedDelay = 5000l)
    @Traced
    @SLA(errorRateThreshold = 0d, metric = MetricValueType.P95, threshold = 500d)
    private void doBatchWork() {
        log.info("Doing background batch work...")
        worker.generateDelay()
        log.info("Done with batch work.")
    }
}
